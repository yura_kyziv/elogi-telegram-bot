<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Plugin;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Mastering\ElogicTelegramBot\Api\Data\DataConfigInterface;
use Mastering\ElogicTelegramBot\Api\TelegramApiInterface;

class OrderAfterSave
{
    /**
     * @var DataConfigInterface
     */
    private DataConfigInterface $helper;

    /**
     * @var TelegramApiInterface
     */
    private TelegramApiInterface $telegramBot;

    /**
     * @param DataConfigInterface $helper
     * @param TelegramApiInterface $telegramBot
     */
    public function __construct(
        DataConfigInterface  $helper,
        TelegramApiInterface $telegramBot
    )
    {
        $this->helper = $helper;
        $this->telegramBot = $telegramBot;
    }

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterSave(OrderRepositoryInterface $orderRepository, OrderInterface $order): OrderInterface
    {
        if ($this->helper->isActive()) {
            if (!$order->getCustomerIsGuest()) {
                $this->telegramBot->sendOrderMessage($order);
            }
        }
        return $order;
    }
}

