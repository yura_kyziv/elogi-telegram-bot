<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Block\Telegram\Config;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Mastering\ElogicTelegramBot\Api\Data\DataConfigInterface;
use Mastering\ElogicTelegramBot\Api\Data\HelperInterface;

/**
 * @api
 * @since 100.0.2
 */
class UserInfo extends Template implements HelperInterface
{
    /**
     * @var Session
     */
    private Session $customerSession;

    /**
     * @var DataConfigInterface
     */
    private DataConfigInterface $helper;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param DataConfigInterface $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        DataConfigInterface $helper,
        array   $data = []
    )
    {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->helper = $helper;
    }

    /**
     * @return Customer
     */
    public function getUser(): Customer
    {
        return $this->customerSession->getCustomer();
    }

    /**
     * @return DataConfigInterface
     */
    public function getHelper(): DataConfigInterface
    {
        return $this->helper;
    }
}


