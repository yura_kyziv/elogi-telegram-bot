<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Block\Telegram\Config;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Mastering\ElogicTelegramBot\Api\Data\DataConfigInterface;
use Mastering\ElogicTelegramBot\Api\Data\HelperInterface;

/**
 * @api
 * @since 100.0.2
 */
class Button extends Template implements HelperInterface
{
    /**
     * @var DataConfigInterface
     */
    private DataConfigInterface $helper;

    /**
     * @param Context $context
     * @param DataConfigInterface $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        DataConfigInterface $helper,
        array   $data = []
    )
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    /**
     * @return DataConfigInterface
     */
    public function getHelper(): DataConfigInterface
    {
        return $this->helper;
    }
}

