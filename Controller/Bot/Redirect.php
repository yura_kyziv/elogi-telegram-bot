<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Controller\Bot;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Redirect extends Action
{

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $url = "https://t.me/kyziv_bot";
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }
}

