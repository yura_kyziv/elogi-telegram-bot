<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Controller\Bot;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\NotFoundException;

class Config extends Action
{
    /**
     * @var Session
     */
    protected Session $_customerSession;

    /**
     * @var Url
     */
    private Url $url;

    /**
     * @var Session
     */
    private Session $customerSession;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param Url $url
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Url     $url
    )
    {
        $this->customerSession = $customerSession;
        $this->url = $url;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws NotFoundException
     */
    public function dispatch(RequestInterface $request): ResponseInterface
    {
        $loginUrl = $this->url->getLoginUrl();

        if (!$this->customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     * Display downloadable links bought by customer
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Telegram Bot Config'));
        $this->_view->renderLayout();
    }
}