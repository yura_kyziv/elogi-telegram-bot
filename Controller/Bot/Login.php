<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Controller\Bot;

use Exception;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\InputMismatchException;

class Login extends Action
{

    /**
     * @var Session
     */
    private Session $customerSession;

    /**
     * @param Context $context
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        Session $customerSession
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
    }

    public function execute()
    {
        try {
            $this->setTelegramId();
            $this->messageManager->addSuccessMessage('good');
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(
                'Error with telegram'
            );
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $url = $this->_url->getUrl('customer/bot/config');
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }

    /**
     * @return void
     * @throws InputException
     * @throws LocalizedException
     * @throws InputMismatchException
     * @throws Exception
     */
    private function setTelegramId(): void
    {
        $request = $this->_request->getParam('id');
        $customer = $this->customerSession->getCustomer();

        $customer->setCustomAttribute('telegram_id', $request);
        $customer->save();
    }
}