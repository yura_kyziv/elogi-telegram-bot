<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Source\Data;

use Magento\Framework\Data\OptionSourceInterface;
use Mastering\ElogicTelegramBot\Api\Data\ButtonStyleInterface;

class ButtonStyle implements OptionSourceInterface
{

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => ButtonStyleInterface::SMALL, 'label' => __('Small')],
            ['value' => ButtonStyleInterface::MEDIUM, 'label' => __('Medium')],
            ['value' => ButtonStyleInterface::LARGE, 'label' => __('Large')],
        ];
    }
}

