<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;

class OrderProcessor
{
    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository
    )
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param OrderInterface $order
     * @return string
     */
    public function getOrderMessage(OrderInterface $order): string
    {
        if ($order->getCreatedAt() == $order->getUpdatedAt()) {
            $body = "You get new order: ";
        } else {
            $body = "Your order was update: ";
        }
        $body .= $this->getOrderInformation($order);
        return $body;
    }

    /**
     * @param OrderInterface $order
     * @return string
     */
    private function getOrderInformation(OrderInterface $order): string
    {
        $result = "\n\n ===========================\n";
        $result .= "\nOrder #: {$order->getIncrementId()}";
        $result .= "\nStatus: {$order->getStatus()}";
        $result .= "\nPrice: {$order->getGrandTotal()} {$order->getOrderCurrencyCode()}";
        return $result;
    }

    /**
     * @param OrderInterface $order
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomer(OrderInterface $order): CustomerInterface
    {
        $customerEmail = $order->getCustomerEmail();
        $customer = $this->customerRepository->get($customerEmail);
        return $customer;
    }
}