<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Helper\Data;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Mastering\ElogicTelegramBot\Api\Data\ConfigInterface;
use Mastering\ElogicTelegramBot\Api\Data\DataConfigInterface;

class Data extends AbstractHelper implements DataConfigInterface
{

    /**
     * @return mixed
     */
    public function isActive()
    {
        return $this->scopeConfig->getValue(ConfigInterface::GENERAL_PATH . ConfigInterface::ACTIVE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getBotName()
    {
        return $this->scopeConfig->getValue(ConfigInterface::WIDGET_PATH . ConfigInterface::NAME, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isShowPhoto()
    {
        return $this->scopeConfig->getValue(ConfigInterface::WIDGET_PATH . ConfigInterface::PHOTO, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getButtonStyle()
    {
        return $this->scopeConfig->getValue(ConfigInterface::WIDGET_PATH . ConfigInterface::STYLE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getBotToken()
    {
        return $this->scopeConfig->getValue(ConfigInterface::GENERAL_PATH . ConfigInterface::TOKEN, ScopeInterface::SCOPE_STORE);
    }

    public function isDebugMode()
    {
        return $this->scopeConfig->getValue(ConfigInterface::GENERAL_PATH . ConfigInterface::DEBUG_MODE, ScopeInterface::SCOPE_STORE);
    }
}

