<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Api\Data;

interface ConfigInterface
{
    const GENERAL_PATH = 'mastering_telegram_bot/general/';
    const WIDGET_PATH = 'mastering_telegram_bot/widget/';

    const ACTIVE = 'active';
    const NAME = 'name';
    const STYLE = 'style';
    const PHOTO = 'photo';
    const TOKEN = 'token';
    const DEBUG_MODE = 'debug';

}

