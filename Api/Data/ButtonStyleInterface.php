<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Api\Data;

interface ButtonStyleInterface
{
    const LARGE = 'large';
    const MEDIUM = 'medium';
    const SMALL = 'small';
}

