<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Api\Data;

interface DataConfigInterface
{
    /**
     * @return mixed
     */
    public function isActive();

    /**
     * @return mixed
     */
    public function getBotName();

    /**
     * @return mixed
     */
    public function isShowPhoto();

    /**
     * @return mixed
     */
    public function getButtonStyle();

    /**
     * @return mixed
     */
    public function getBotToken();

    /**
     * @return mixed
     */
    public function isDebugMode();
}