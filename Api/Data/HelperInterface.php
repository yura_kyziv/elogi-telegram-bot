<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Api\Data;

interface HelperInterface
{

    /**
     * @return DataConfigInterface
     */
    public function getHelper(): DataConfigInterface;
}