<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Api;

use Magento\Sales\Api\Data\OrderInterface;

interface TelegramApiInterface
{
    /**
     * @param OrderInterface $order
     * @return bool
     */
    public function sendOrderMessage(OrderInterface $order): bool;
}
