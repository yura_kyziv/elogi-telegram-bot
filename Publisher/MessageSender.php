<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Publisher;

use Magento\Framework\MessageQueue\PublisherInterface;

class MessageSender
{
    const TOPIC_NAME = "telegramMessage.topic";

    /**
     * @var PublisherInterface
     */
    private PublisherInterface $publisher;

    /**
     * @param PublisherInterface $publisher
     */
    public function __construct(
        PublisherInterface $publisher
    )
    {
        $this->publisher = $publisher;
    }

    /**
     * @param array $data
     * @return mixed|null
     */
    public function publish(array $data)
    {
        $data = json_encode($data);
        return $this->publisher->publish(self::TOPIC_NAME, $data);
    }
}