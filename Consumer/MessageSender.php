<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Consumer;

class MessageSender
{

    /**
     * @param $operation
     * @return void
     */
    public function messageProcessor($operation)
    {
        $operation = json_decode($operation);
        $apiToken = $operation->token;
        $data = $operation->message;
        $response = file_get_contents("https://api.telegram.org/bot$apiToken/sendMessage?" . http_build_query($data));
    }
}