<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Mastering\ElogicTelegramBot\Api\Data\DataConfigInterface;
use Mastering\ElogicTelegramBot\Api\TelegramApiInterface;
use Mastering\ElogicTelegramBot\Helper\OrderProcessor;
use Mastering\ElogicTelegramBot\Publisher\MessageSender;

class TelegramBot extends AbstractTelegramBot implements TelegramApiInterface
{
    /**
     * @var DataConfigInterface
     */
    private DataConfigInterface $helper;

    /**
     * @var OrderProcessor
     */
    private OrderProcessor $orderProcessor;

    /**
     * @var MessageSender
     */
    private MessageSender $messageSender;

    /**
     * @param DataConfigInterface $helper
     * @param OrderProcessor $orderProcessor
     * @param MessageSender $messageSender
     */
    public function __construct(
        DataConfigInterface $helper,
        OrderProcessor      $orderProcessor,
        MessageSender       $messageSender
    )
    {
        $this->helper = $helper;
        $this->orderProcessor = $orderProcessor;
        $this->messageSender = $messageSender;
    }

    /**
     * @param OrderInterface $order
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function sendOrderMessage(OrderInterface $order): bool
    {
        $customer = $this->orderProcessor->getCustomer($order);
        $customerTelegramId = $customer->getCustomAttribute('telegram_id')->getValue();
        if ($customerTelegramId) {
            $apiToken = $this->helper->getBotToken();
            $message = $this->orderProcessor->getOrderMessage($order);
            $result = $this->sendMessage($apiToken, $customerTelegramId, $message);
            return $result;
        }
        return false;
    }

    /**
     * @param string $apiToken
     * @param string $customerTelegramId
     * @param string $message
     * @return bool
     */
    protected function sendMessage(string $apiToken, string $customerTelegramId, string $message): bool
    {
        try {
            $data = [
                'token' => $apiToken,
                'message' => [
                    'chat_id' => $customerTelegramId,
                    'text' => $message
                ]
            ];
            $this->messageSender->publish($data);
            /*


            $result = json_decode($response);
            $status = $result->ok;
            return $status;*/
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }
}

