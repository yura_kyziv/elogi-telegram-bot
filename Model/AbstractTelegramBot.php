<?php
declare(strict_types=1);

namespace Mastering\ElogicTelegramBot\Model;

abstract class AbstractTelegramBot
{
    /**
     * @param string $apiToken
     * @param string $customerTelegramId
     * @param string $message
     * @return bool
     */
    protected abstract function sendMessage(string $apiToken, string $customerTelegramId, string $message): bool;

}